//
//  iOS7TestsTests.m
//  iOS7TestsTests
//
//  Created by Christopher Denter on 8/16/13.
//  Copyright (c) 2013 Christopher Denter. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface iOS7TestsTests : XCTestCase

@end

@implementation iOS7TestsTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
