//
//  BAScrollingTestViewController.m
//  iOS7Tests
//
//  Created by Christopher Denter on 8/16/13.
//  Copyright (c) 2013 Christopher Denter. All rights reserved.
//

#import "BAScrollingTestViewController.h"

@interface BAScrollingTestViewController ()

@end

@implementation BAScrollingTestViewController {
    UIColor *_color;
    UIScrollView *_scrollView;
}

- (id)initWithColor:(UIColor *)color
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        _color = color;
        self.title = _color.description;
    }
    return self;
}

- (void)loadView
{
    UIView *coloredThingieSoWeCanSeeScrolling = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 15)];
    coloredThingieSoWeCanSeeScrolling.backgroundColor = _color;

    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [_scrollView addSubview:coloredThingieSoWeCanSeeScrolling];
    _scrollView.contentSize = _scrollView.bounds.size;

    self.view = _scrollView;
}

@end
