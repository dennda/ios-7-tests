//
//  BAAppDelegate.h
//  iOS7Tests
//
//  Created by Christopher Denter on 8/16/13.
//  Copyright (c) 2013 Christopher Denter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
