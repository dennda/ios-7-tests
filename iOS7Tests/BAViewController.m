//
//  BAViewController.m
//  iOS7Tests
//
//  Created by Christopher Denter on 8/16/13.
//  Copyright (c) 2013 Christopher Denter. All rights reserved.
//

#import "BAViewController.h"
#import "BAScrollingTestViewController.h"

@interface BAViewController ()

@end

@implementation BAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIViewController* (^ScrollingVC)(UIColor *) = ^(UIColor *color){
        UIViewController *vc = [[BAScrollingTestViewController alloc] initWithColor:color];

        // Wrap it in a nav vc
        vc = [[UINavigationController alloc] initWithRootViewController:vc];

        return vc;
    };

    UITabBarController *tabController = [[UITabBarController alloc] init];
    tabController.viewControllers = @[ScrollingVC([UIColor redColor]), ScrollingVC([UIColor greenColor])];
    tabController.view.backgroundColor = [UIColor lightGrayColor];

    UIViewController *rootVC = tabController;

    rootVC.view.frame = self.view.bounds;
    [self addChildViewController:rootVC];
    [self.view addSubview:rootVC.view];
    [rootVC didMoveToParentViewController:self];
}

@end
